<div class="progress-meter">
%pages = len(paginas)
%for i in range(1,pages+1):
	<div class="bullet" id="b{{i}}">{{i}}</div>
%end
</div>
<form method="post">
<div class="screens">
	<!-- Home Screen -->
	<div class="screen" id="greetings">
		<h1>{{!home['titulo']}}</h1>
		<h3>{{!home['subtitulo']}}</h3>
		<button type="button" class="btn btn-large btn-primary" onclick="next()">Responder &raquo;</button>
		<p><small>{{!home['texto']}}</small></p>
	</div>
	<!-- Screens -->
%for i in range(0,pages):
	%screen = paginas[i]
	<div class="screen" id="q{{i}}">
		<h2>{{!screen['pergunta']}}</h2>
		<p><small>{{!screen['dica']}}</small></p>
		<!-- Itens de resposta -->
	
	%if screen['tipo'] == 'opcoes':
		%for y in range(0,len(screen['itens'])):
			%item = screen['itens'][y]
		<div class="label-box"><label>
			<img src="{{item['icone']}}" />
			<div>{{!item['titulo']}}</div>
			<input type="radio" id="{{screen['nome']}}" name="{{screen['nome']}}" onclick="next()" value="{{item['valor']}}">
			<div><small>{{!item['descricao']}}</small></div>
		</label></div>
		%end
	%elif screen['tipo'] == 'formulario':
		%for y in range(0,len(screen['itens'])):
			%item = screen['itens'][y]
		<label for="{{screen['nome']}}_{{y}}">{{item['titulo']}}</label>
		<input type="{{item['tipo_formulario']}}" id="{{screen['nome']}}_{{y}}" name="{{screen['nome']}}_{{y}}" placeholder="{{item['descricao']}}" value="{{item['valor']}}"/>
		%end
	%elif screen['tipo'] == 'texto':
		%item = screen['itens'][0]
		<textarea id="{{screen['nome']}}" name="{{screen['nome']}}" placeholder="{{item['descricao']}}">{{item['valor']}}</textarea>
	%else:
		<p class="error">A pergunta não foi corretamente formulada.</p>
	%end

		<div class="button-area">
			<button type="button" class="btn" onclick="back()">Voltar</button>
		%if not screen['tipo'] == 'opcoes':
			<button type="button" class="btn btn-primary" onclick="next()">Avançar</button>
		%end
		</div>
	</div>
%end
	<!-- End Screen -->
	<div class="screen" id="thanks">
		<h1>{{!end['titulo']}}</h1>
		<h3>{{!end['subtitulo']}}</h3>
		<p>&nbsp;</p>
		<p><small>{{!end['texto']}}</small></p>
		<div class="button-area">
			<button type="button" class="btn" onclick="back()">Voltar</button>
			<button type="submit" class="btn btn-large btn-primary" onclick="next()">Concluir!</button>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
// Variável que contém o nome das páginas
selectors = [
	"greetings",
%for i in range(0,pages):
	"q{{i}}",
%end
	"thanks"
];
</script>
%rebase template