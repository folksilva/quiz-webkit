<!DOCTYPE html>
<html lang="pt">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<!--<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />-->
		<meta name="viewport" content="width=1024, height=768" />
		<meta name="author" content="Luiz Fernando da Silva <lfsilva@sccorinthians.com.br>" />
		<meta name="copyright" content="Sport Club Corinthians Paulista" />
		<meta name="description" content="Sistema Gerenciador de contratos" />
		<title>Quiz do visitante</title>
		<link rel="stylesheet" href="/static/css/bootstrap.min.css">
		<link rel="stylesheet" href="/static/css/quiz.css">
	</head>
	<body>
		
		%include

		<!-- Application logic -->
		<script type="text/javascript" src="/static/js/jquery.js"></script>
		<script type="text/javascript" src="/static/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/static/js/quiz.js"></script>
	</body>
</html>
