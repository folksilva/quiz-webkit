var index = 0;
$(document).ready(function(){
	$('html, body').animate({
		scrollTop: $("#" + selectors[index]).offset().top
	}, 1000);
	$(".progress-meter").hide();
	$("input[type='radio']").live('click', function(){
		$("input[name=" + $(this).attr("name") + "]").parent().parent().removeClass("selected");
		$(this).parent().parent().addClass("selected");
	});
});
function next() {
	index ++;
	if (index == selectors.length) index = 0;
	$('html, body').animate({
		scrollTop: $("#" + selectors[index]).offset().top
	}, 1000);
	meter(index);
}
function back() {
	index --;
	if (index < 0) index = selectors.length-1;
	$('html, body').animate({
		scrollTop: $("#" + selectors[index]).offset().top
	}, 1000);
	meter(index);
}
function meter(i) {
	if (i == 0 || i >= selectors.length-1) {
		$(".progress-meter").fadeOut();
	} else {
		$(".bullet").removeClass("on");
		$("#b"+i).addClass("on");
		$(".progress-meter").fadeIn();
	}
}