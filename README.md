
quiz-webkit 2.0
===============

Versão para webkit do aplicativo de perguntas aos visitantes do clube, para uso em um computador ou Raspberry Pi.

AUTHOR: Luiz Fernando da Silva <<lfsilva@sccorinthians.com.br>><br>
**CC-BY-SA**:
Este trabalho está licenciado sob uma Licença Creative Commons Atribuição-CompartilhaIgual 3.0 Não Adaptada. Para ver uma cópia desta licença, visite [http://creativecommons.org/licenses/by-sa/3.0/](http://creativecommons.org/licenses/by-sa/3.0/).


DESCRIÇÃO
---------
É preciso o python para executar o servidor<br>
O servidor roda com o microframework Bottle [http://bottlepy.org/](http://bottlepy.org/)<br>
Resolução necessária para o template: 1024x768px<br>
Para executar o modo kiosk é preciso ter o Google© Chrome ou Chromium atualizado (Recomendo desabilitar o auto-preenchimento de campos)


COMO USAR:
----------
1. Extraia todo o conteúdo em um diretório de sua preferência
2. Abra um console e direcione para o diretório escolhido:

>cd /caminho/para/o/diretorio

3. Inicie o servidor com o comando:

>*Linux*<br>
>python server.py<br>
>*Windows*<br>
>C:/caminho/para/o/python.exe app.py

4. Inicie o Google© Chrome no modo kiosk:

>*Linux*<br>
>chromium-browser --kiosk http://localhost:8888/
>*Windows*<br>
>C:/caminho/para/o/chrome.exe --kiosk http://localhost:8888/

5. Após a coleta dos dados, volte ao terminal onde foi iniciado o servidor e pressione CTRL+C
6. Copie o arquivo data.csv para um dispositivo removível ou compartilhamento de rede, abra-o com um software de planilha eletrônica


RESOLUÇÂO DE PROBLEMAS:
-----------------------
* O servidor travou! E agora?
	Volte ao terminal onde foi iniciado o servidor, pressione CTRL+C e repita o procedimento para iniciar o servidor
* O botão voltar/avançar está fora da tela.
	Verifique se a resolução do monitor é de 1360x768px
* O servidor está ativo mas quando acesso a página ocorre um erro 404
	Verifique se iniciou o servidor no mesmo diretório onde estão os arquivos do sistema
* Ao abrir a página ou enviar os dados surge um erro 500
	Volte ao terminal onde foi iniciado o servidor e tire um print de tela, envie para lfsilva@sccorinthians.com.br. Em resposta será enviada uma solução.
* Iniciei o Google© Chrome no modo kiosk mas ele não ficou em tela cheia
	Pressione a tecla F11 para entrar no modo kiosk tela cheia

CONFIGURAÇÂO:
-------------
Utilize o arquivo quiz.json para configurar as páginas do Quiz

* home: Contém as configurações da página inicial de boas vindas
	* titulo: O título principal do Quiz
	* subtitulo: O subtitulo do Quiz
	* texto: Um texto adicional explicativo para a home
* end: Contém as configurações da página de confirmação e envio do Quiz
	* titulo: O título da página. Ex.: Obrigado!
	* subtitulo: O subtitulo da página
	* texto: Um texto adicional explicativo para a página
* paginas: Uma lista que contém as configurações das páginas de perguntas
	* nome: O nome da pergunta ou da página para identificação
	* pergunta: A pergunta da página
	* dica: Um texto de ajuda para responder a pergunta
	* tipo: O tipo de resposta da pergunta, pode ser:
		* opcoes: A resposta será de escolha de uma opção
		* formulario: A resposta é um formulário que deve ser preenchido
		* texto: A resposta é um texto longo que o pode deve escrever
	* itens: Lista que contém as configurações dos itens de resposta, quando o tipo da pergunta é texto, apenas o primeiro item da lista é considerado
		* icone: Imagem de icone para a resposta, apenas para pergunta do tipo opcoes
		* titulo: Titulo da resposta ou do campo de formulário
		* descricao: Descrição da resposta ou dica do campo de formulário
		* valor: O valor da opção ou valor padrão do campo de formulário
		* tipo_formulário: O tipo de campo a exibir, apenas quanto o tipo de pergunta é formulario, pode ser:
			* text: Um campo de texto
			* email: Um campo de e-mail
			* number: Um campo numérico
			* date: Um campo de data
			* time: Um campo de hora

A quantidade de itens em uma página é definida pelo template utilizado

TEMPLATES
=========
Edite o arquivo template.tpl na pasta template e configure as imagens e estilos CSS nos arquivos da pasta static. O template será incluído e processado pelo aplicativo. Veja os exemplos para se orientar.
