# -*- coding: utf-8 -*-
#
# Simples servidor HTTP para o aplicativo Quiz do visitante
# author: Luiz Fernando da Silva <lfsilva@sccorinthians.com.br>

import os, quiz, bottle
from bottle import *

bottle.debug(True)
bottle.TEMPLATE_PATH = [os.curdir + os.sep + "template"]

@get('/')
def get_quiz():
	config = quiz.get_config()
	return template("quiz", **config)

@post('/')
def post_quiz():
	config = quiz.get_config()
	resposta = {"data":datetime.now()}
	for pergunta in config["paginas"]:
		key = pergunta['nome']
		if pergunta['tipo'] == "formulario":
			for i in range(0,len(pergunta["itens"])):
				skey = "%s_%s" % (key,i)
				resposta[skey] = request.forms[skey]
		else:
			resposta[key] = request.forms[key]
	quiz.save_data(resposta)
	redirect("/")

@route('/static/<filepath:path>')
def static(filepath):
	root = os.curdir + os.sep + "static"
	return static_file(filepath, root=root)

run(host='localhost', port=8000, reloader=True)