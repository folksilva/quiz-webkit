# -*- coding: utf-8 -*-
#
# Gerador de páginas de Quiz a partir de template HTML e arquivo JSON
# author: Luiz Fernando da Silva <lfsilva@sccorinthians.com.br>

import json, os, csv
from os import curdir, sep

# Se alterou o nome do arquivo de configuração altere aqui também

JSON_CONFIG_FILE = "quiz.json"

def get_config():
	config_path = curdir + sep + JSON_CONFIG_FILE
	if not os.path.exists(config_path):
		raise Exception("Impossível encontrar o arquivo de configuração")
	else:
		return json.load(open(config_path,'rb'))

def save_data(data):
	data_exists = os.path.exists('data.csv')
	writer = csv.DictWriter(open('data.csv','a'), fieldnames=data.keys(), delimiter=";", quotechar='"', quoting=csv.QUOTE_ALL)
	if not data_exists: 
		writer.writeheader()
	writer.writerow(data)